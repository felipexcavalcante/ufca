# -*- coding: utf-8
from django.db import models
from base.models import Setor, Pessoa, Fornecedor
from django.db.models import Sum

from decimal import Decimal

#==========================================================
# Unidade
#==========================================================
class Unidade(models.Model):
    descricao = models.CharField(u'Descrição', max_length=50, unique=True)
    simbolo = models.CharField(u'Símbolo', max_length=15, unique=True)

    class Meta:
        ordering = ['descricao']

    def __unicode__(self):
        return self.simbolo



#==========================================================
# Material
#==========================================================
class MaterialManager(models.Manager):
    def get_query_set(self):
        query_set = super(MaterialManager, self).get_query_set()
        
        '''	
		return query_set.extra(
			select = {
				'_quant_entrada': """select sum(quant_atendida) from almoxarifado_itemdeentrada where  almoxarifado_itemdeentrada.material_id = almoxarifado_material.id""",
				'_quant_saida': """select sum(quant_atendida) from almoxarifado_itemdesaida where  almoxarifado_itemdesaida.material_id = almoxarifado_material.id"""
			}
		)'''
	
class Material(models.Model):
	codigo = models.CharField(u'código', max_length=50, unique=True)
	descricao = models.CharField(u'descrição', max_length=50, unique=True)
	quantidade_minima = models.PositiveIntegerField(u'quantidade mínima', null=True, blank=True)
	quantidade_ideal = models.PositiveIntegerField(null=True, blank=True)
	unidade = models.ForeignKey(Unidade)
	
	#objects = MaterialManager()
	'''
	def get_quant_entrada(self):
		return self._quant_entrada or 0

	def get_quant_saida(self):
		return self._quant_saida or 0

	def quant_em_estoque(self):
		return self.get_quant_entrada() - self.get_quant_saida()
	
	def quant_OK(self):
		if self.quantidade_ideal == 0:
			return True
		else:
			return self.quant_em_estoque() < self.quantidade_ideal
	'''	
	class Meta:
		verbose_name = 'material'
		verbose_name_plural = 'materiais'
		ordering = ['descricao']

	def __unicode__(self):
		return u'%s - %s - UNIDADE: %s' % (self.codigo, self.descricao, self.unidade.simbolo)


#==========================================================
# Entrada
#==========================================================
TIPO_DE_ENTRADA = (
    ('C', u'Compra'),
    ('D', u'Doação'),
    ('A', u'Avulsa'),
    ('V', u'Devolução'),
)
class Entrada(models.Model):
    data = models.DateField()
    empenho = models.CharField(u'empenho', max_length=15, null=True, blank=True)
    processo = models.CharField(u'processo', max_length=15, null=True, blank=True)
    fornecedor = models.ForeignKey(Fornecedor, null=True, blank=True)
    tipo = models.CharField(max_length=1, choices=TIPO_DE_ENTRADA, default='A')
    observacao = models.TextField(u'Observação', null=True, blank=True)
    
    def data_fmt(self):
        data_fmt = self.data.strftime("%d/%m/%Y - %Hh%mm")
        return data_fmt

    data_fmt.short_description = 'Data formatada' 
        

    def _get_total(self):
        total = 0
        for item in self.itemdeentrada_set.all():
            total = total + item.total
        
        return total
        
    total = property(_get_total)

    created = models.DateTimeField(auto_now_add=True, editable=False)
    modified = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        ordering = ['-data', 'fornecedor__nome']

    def __unicode__(self):
        return self.data.__str__() + ' - ' + self.fornecedor.nome


#==========================================================
# Item da Entrada
#==========================================================
class ItemDeEntrada(models.Model):
    material = models.ForeignKey(Material)
    entrada = models.ForeignKey(Entrada)
    quantidade = models.PositiveIntegerField()
    valor = models.DecimalField(max_digits=7, decimal_places=2)
	
    def _get_total(self):
        return self.valor*self.quantidade
        
    total = property(_get_total)

    created = models.DateTimeField(auto_now_add=True, editable=False)
    modified = models.DateTimeField(auto_now=True, editable=False)

    def __unicode__(self):
        return self.material.descricao

    class Meta:
        verbose_name = 'Item'
        verbose_name_plural = 'Itens'
        ordering = ['material__codigo']



#==========================================================
# Solicitante
#==========================================================
class Solicitante(models.Model):
    pessoa = models.ForeignKey(Pessoa)
    setor = models.ForeignKey(Setor)
    
    class Meta:
        ordering = ['pessoa__nome']

    def __unicode__(self):
        return u'%s - %s' % (self.pessoa.nome, self.setor.descricao)
        
#==========================================================
# Saída
#==========================================================
TIPO_DE_SAIDA = (
    ('C', u'Consumo'),
    ('X', u'Baixa'),
)

ESTADO_DA_SAIDA = (
    ('R', u'Recebido'),
    ('S', u'Separado'),
    ('E', u'Entregue'),
)

class Saida(models.Model):
    data = models.DateField()
    solicitante = models.ForeignKey(Solicitante,)
    tipo = models.CharField(max_length=1, choices=TIPO_DE_SAIDA, default='C')
    estado = models.CharField(max_length=1, choices=ESTADO_DA_SAIDA, default='R')
    observacao = models.TextField(u'Observação', null=True, blank=True)
    
    def pessoa_solicitante(self):
    	return self.solicitante.pessoa.nome
	
    def __unicode__(self):
        return u'%s - %s' % (self.solicitante, self.setor)
    
    class Meta:
        verbose_name = u'saída'
        verbose_name_plural = u'saídas'
        ordering = ['-data', 'solicitante__setor__descricao']

#==========================================================
# Item da Saída
#==========================================================
class ItemDeSaida(models.Model):
    material = models.ForeignKey(Material)
    saida = models.ForeignKey(Saida)
    quant_solicitada = models.PositiveIntegerField()
    quant_atendida = models.PositiveIntegerField(null=True, blank=True)
    
    def _quant_em_estoque(self):
    	self.material.quant_em_estoque()
	
	quant_em_estoque = property(_quant_em_estoque)
	
    def __unicode__(self):
        return self.material.descricao

    class Meta:
        verbose_name = 'Item'
        verbose_name_plural = 'Itens'

