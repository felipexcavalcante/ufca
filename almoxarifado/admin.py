# -*- coding: utf-8
from django.contrib import admin
from django.contrib.admin.options import ModelAdmin
from almoxarifado.models import *
from django.core.exceptions import PermissionDenied
from django.contrib.admin.views.main import ChangeList
from re import template
#from ajax_select.fields import AutoCompleteSelectField
#from ajax_select import make_ajax_form

LIST_PER_PAGE = 15

class NewAdmin(admin.ModelAdmin):
    list_per_page = LIST_PER_PAGE
    actions = None

#admin.site.disable_action('delete_selected')

class UnidadeAdmin(NewAdmin):
    model = Unidade
    list_display = ('descricao', 'simbolo')
    search_fields = ('descricao', 'simbolo')


class MaterialAdmin(NewAdmin):
    model = Material
    list_display = ( 'codigo', 'descricao', 'unidade', )
    search_fields = ('codigo', 'descricao', )
    list_filter = ('unidade',)
    list_display_links = ('codigo', 'descricao', )
    ordering = ['codigo']

#	actions = ['teste']
    '''
	
	def teste(self, request, queryset):
		pass
	'''


class AdminItemDeEntrada(admin.TabularInline):
	model = ItemDeEntrada
	extra = 10
	
class AdminEntrada(NewAdmin):
	model = Entrada
	inlines = [AdminItemDeEntrada]
	list_display = ('processo', 'empenho', 'tipo', 'data', 'fornecedor', 'data_fmt')
	search_fields = ('data', 'fornecedor')
	list_filter = ('tipo', 'data','fornecedor')
	readonly_fields = ('total',)

	date_hierarchy = 'data'
	

class AdminItemDeSaida(admin.TabularInline):
	model = ItemDeSaida
	extra = 3
#	ordering = ['descricao']

class _AdminItemDeSaida(admin.TabularInline):
	model = ItemDeSaida
#	form = make_ajax_form(ItemDeSaida,dict(material='material'))

	
class AdminSaida(NewAdmin):
	model = Saida
	inlines = [AdminItemDeSaida]
	list_display = ('tipo', 'estado', 'data', 'solicitante',)
	search_fields = ('solicitante__pessoa__nome', 'solicitante__setor__descricao','itemdesaida__material__descricao', 'itemdesaida__material__codigo')
	list_filter = ('tipo', 'data',)
	date_hierarchy = 'data'

   
admin.site.register(Unidade, UnidadeAdmin)
admin.site.register(Material, MaterialAdmin)
admin.site.register(Solicitante)
admin.site.register(Entrada, AdminEntrada)
admin.site.register(Saida, AdminSaida)

