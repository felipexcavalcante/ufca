# -*- coding: utf-8
from django.db import models
from base.models import Pessoa, Setor, Local


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
### Definição de novos tipos de campos
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
###  Modelos da aplicação
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
###  Classificação
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
class Classificacao(models.Model):
    classificacao = models.CharField(u'classificação', max_length=15, unique=True)
    descricao = models.CharField(max_length=100, verbose_name='descrição')
    complemento = models.TextField(null=True, blank=True)
    
    created = models.DateTimeField(auto_now_add=True, editable=False)
    modified = models.DateTimeField(auto_now=True, editable=False)
    
    class Meta:
        ordering = ['classificacao']
        verbose_name = u'classificação'
        verbose_name_plural = u'classificações'
        
    def __unicode__(self):
        return u'%s - %s' % (self.classificacao, self.descricao)
#        return u'%s - %s' % (self.classificacao, self.descricao)
	

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
###  Incorporação
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
class Incorporacao(models.Model):
    TIPO = (('C', u'Compra'), ('D', u'Doação'))
    
    tipo = models.CharField(choices=TIPO, max_length=1)
    empenho = models.CharField(max_length=15, null=True, blank=True)
    processo = models.CharField(max_length=15)
    documento = models.CharField(max_length=15)
    datadoemp = models.DateField(u'Data do Empenho')
    datadainc = models.DateField(u'Data da Incorporação', null=True, blank=True)
    observacao = models.TextField(null=True, blank=True, verbose_name='observação')
    
    cadastrouositens = models.BooleanField(u'itens já incorporados?', default=False)

    def _get_total(self):
        total = 0
        for item in self.itemdeincorporacao_set.all():
            total = total + item.total
        
        return total
        
    total = property(_get_total)


    created = models.DateTimeField(auto_now_add=True, editable=False)
    modified = models.DateTimeField(auto_now=True, editable=False)
    
    class Meta:
        unique_together = (("empenho", "processo", "documento"),)
        verbose_name = u'incorporação'
        verbose_name_plural = u'incorporações'
        
          
    def __unicode__(self):
        return u'%s - %s - %s' % (self.empenho, self.processo, self.documento)
        


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
###  Item de Incorporação
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
class ItemDeIncorporacao(models.Model):
    incorporacao = models.ForeignKey('Incorporacao')
    classificacao = models.ForeignKey('Classificacao', verbose_name=u'classificação')
    valor = models.FloatField()
    quantidade = models.IntegerField()
    #total = models.FloatField()

    created = models.DateTimeField(auto_now_add=True, editable=False)
    modified = models.DateTimeField(auto_now=True, editable=False)

    
    def _get_total(self):
        return self.valor*self.quantidade
        
    total = property(_get_total)
    
        
    class Meta:
        verbose_name = u'Item da Incorporação'
        verbose_name_plural = u'Itens da Incorporação'
        
    def __unicode__(self):
        return u'%s - %s' % (self.classificacao.classificacao, self.classificacao.descricao)

    '''       
    def save(self, *args, **kwargs):
        self.total = self.valor*self.quantidade
        super(ItemDeIncorporacao, self).save(*args, **kwargs)
    '''
    
        


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
###  Material
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
class Material(models.Model):
    ESTADO = (('E', u'Excelente'), ('B', u'Bom'), ('D', u'Com Defeito'), ('C', u'Desmembrado'), ('X', u'Extraviado'), ('I', u'Inservível'), )
    
    
    numero = models.CharField(max_length=6, verbose_name='número', unique=True,)
    classificacao = models.ForeignKey('Classificacao', verbose_name=u'classificação')
    setorUFC = models.ForeignKey('SetorUFC', verbose_name='Setor UFC', null=True, blank=True)
    incorporacao = models.ForeignKey('Incorporacao', verbose_name=u'incorporação')
    valor = models.DecimalField(max_digits=17, decimal_places=2, )
    estado = models.CharField(choices=ESTADO, max_length=1)
    observacao = models.TextField(null=True, blank=True, verbose_name='observação')
    
    created = models.DateTimeField(auto_now_add=True, editable=False)
    modified = models.DateTimeField(auto_now=True, editable=False)

    
    @staticmethod
    def proxnumero():
        ultimostr = Material.objects.all().aggregate(models.Max('numero'))['numero__max']
        ultimo = 0
        try:
            ultimo = int(ultimostr)
        except:
            pass
        
        proximo = '%6d' % (ultimo+1)
        proximo = proximo.replace(' ', '0')
        return proximo
    
    
    @staticmethod
    def adicionaum(itemDeIncorporacao):
        m = Material(numero=Material.proxnumero(), classificacao=itemDeIncorporacao.classificacao, incorporacao=itemDeIncorporacao.incorporacao, valor=itemDeIncorporacao.valor, estado=Material.ESTADO[0][0] );
        m.save()
    
    @staticmethod
    def adiciona(itemDeIncorporacao):
        for i in range(itemDeIncorporacao.quantidade):
            Material.adicionaum(itemDeIncorporacao)
            
    
    def local(self):
        if self.transf:
            return self.transf.local
        else:
            return None
		
    class Meta:
        verbose_name_plural = u'Materiais'
        ordering = ['numero']


    def __unicode__(self):
        return u'%s - %s' % (self.numero, self.classificacao.descricao)

		
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
###  Setor do bem na UFC
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
class SetorUFC(models.Model):
	sigla = models.CharField(max_length=5,)
	descricao = models.CharField(max_length=100, verbose_name='descrição')

	class Meta:
		verbose_name = u'Setor UFC'
		verbose_name_plural = u'Setores UFC'

	def __unicode__(self):
		return '%s - %s' % (self.sigla, self.descricao)
		
"""
class Movimentacao(models.Model):

    TIPO = (('T', 'Transferência'), ('E', 'Empréstimo'))

    data = models.DateField(verbose_name='data')
    setor = models.ForeignKey('base.Setor')
    local = models.ForeignKey('base.Local')
    responsavel = models.ForeignKey('base.Pessoa', related_name='resp', verbose_name=u'responsável', help_text=u'Novo responsável')
    finalizada = models.BooleanField(default=False, verbose_name='operação concluída', help_text=u'Operação concluída significa que o termo da movimentação já foi assinado.')
    tipo = models.CharField(choices=TIPO, max_length=1)
    devolucao_prevista = models.DateField(verbose_name='previsão de devolução', null=True, blank=True)
    devolucao_efetiva = models.DateField(verbose_name='devolução efetiva', null=True, blank=True)

    observacao = models.TextField(verbose_name='observação', null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    modified = models.DateTimeField(auto_now=True, editable=False)

    def add_material(self, numero):
        material = Material.objects.get(numero=numero)
        item = ItemTransferencia(material=material)
        self.itemtransferencia_set.add(item)

    class Meta:
        verbose_name = u'Movimentação'
        verbose_name_plural = u'Movimentações'

    def __unicode__(self):
        return u'%s - %s - %s' % (self.data, self.responsavel, self.local)


class ItemDeMovimentacao(models.Model):
	movimentacao = models.ForeignKey('Movimentacao', verbose_name='movimentação')
	material = models.ForeignKey('Material')
	
	class Meta:
		verbose_name = u'Item'
		verbose_name_plural = u'Itens'

	def __unicode__(self):
		return u'%s' % self.material


"""


