# -*- coding: utf-8
from django.db import models

class Fornecedor(models.Model):
	cnpj = models.CharField(max_length=17, unique=True, verbose_name='CNPJ')
	descricao = models.CharField(max_length=100, unique=True, verbose_name='descrição')
	telefones = models.CharField(max_length=30)
	email = models.CharField(u'e-mail', max_length=100, null=True, blank=True)

	class Meta:
		verbose_name_plural = u'Fornecedores'

	def __unicode__(self):
		return self.descricao


class Tipo(models.Model):
	classificacao = models.CharField(u'classificação', max_length=15, unique=True)
	descricao = models.CharField(max_length=100, verbose_name='descrição')
	complemento = models.TextField(null=True, blank=True)
	
	class Meta:
		ordering = ['classificacao']
		
	def __unicode__(self):
		return u'%s - %s' % (self.classificacao, self.descricao)


class Empenho(models.Model):
    TIPO = (('O', u'Ordinário'), ('G', u'Global'), ('E', u'Estimativo'), )

    emissao = models.DateField()
    numero = models.CharField(u'número', max_length=12, unique=True)
    especie = models.CharField(u'espécie', max_length=100)
    emitente = models.CharField(max_length=100)
    cnpj = models.CharField(max_length=17, unique=True, verbose_name='CNPJ')
    credor = models.ForeignKey('Fornecedor')
    endereco = models.CharField(u'endereço', max_length=100)

    tipo = models.CharField(choices=TIPO, max_length=1)


class ItemDeEmpenho(models.Model):
    empenho = models.ForeignKey('Empenho')
    tipo = models.ForeignKey('Tipo')
    quantidade = models.IntegerField(null=False, blank=False)

	

class Incorporacao(models.Model):
	processo = models.CharField(max_length=15)
	empenho = models.ForeignKey('Empenho')
	documento = models.CharField(max_length=15)
	data = models.DateField()

	class Meta:
		unique_together = (("processo", "empenho", "documento"),)
		verbose_name = u'Incorporação'
		verbose_name_plural = u'Incorporações'

	def __unicode__(self):
		return u'%s - %s - %s' % (self.processo, self.empenho, self.documento)



class Material(models.Model):
    ESTADO_DO_BEM = (('E', u'Excelente'), ('B', u'Bom'), ('D', u'Com Defeito'), ('C', u'Desmembrado'), ('X', u'Extraviado'), ('I', u'Inservível'), )

    numero = models.CharField(max_length=6, verbose_name='número', unique=True)
    tipo = models.ForeignKey('Tipo')
    valor = models.DecimalField(max_digits=17, decimal_places=2)
    setorUFC = models.ForeignKey('SetorUFC', verbose_name='Setor UFC', null=True, blank=True)
    incorporacao = models.ForeignKey('Incorporacao', verbose_name='incorporação')
    estado = models.CharField(choices=ESTADO_DO_BEM, max_length=1)
    observacao = models.TextField(null=True, blank=True, verbose_name='observação')
    created = models.DateTimeField(auto_now_add=True, editable=False)
    modified = models.DateTimeField(auto_now=True, editable=False)

	
    def incorporacao_data(self):
        return self.incorporacao.data
		
    def transf_(self):
        from django.db import connection, transaction
        cursor = connection.cursor()
        cursor.execute('''select patrimonio_transferencia.id from patrimonio_material, patrimonio_transferencia , patrimonio_itemtransferencia 
                        where patrimonio_transferencia.id = patrimonio_itemtransferencia.transferencia_id
						  and patrimonio_itemtransferencia.material_id = patrimonio_material.id
						  and patrimonio_material.id = %d
						  %and patrimonio_transferencia.devolucao_efetiva is NULL
						order by patrimonio_transferencia.modified desc''' , [self.id])
        row = cursor.fetchone()
        transf_id = row[0]
        return Transferencia.objects.get(id=transf_id)
		
    def transf(self):
        return None
        '''
        tt = self.itemtransferencia_set.filter(material=self).order_by('-id')
        tr = None
        if tt:
            for t in tt:
                if t.transferencia.devolucao_efetiva == None:
                    return t.transferencia
            return tr
        else:
            return tr'''

    def local(self):
        if self.transf:
            return self.transf.local
        else:
            return None
		
    class Meta:
        verbose_name_plural = u'Materiais'
        ordering = ['numero']


        def __unicode__(self):
            return u'%s - %s' % (self.numero, self.tipo.descricao)

		
class SetorUFC(models.Model):
    sigla = models.CharField(max_length=5,)
    descricao = models.CharField(max_length=100, verbose_name='descrição')

    class Meta:
        verbose_name = u'Setor UFC'
        verbose_name_plural = u'Setores UFC'

    def __unicode__(self):
        return '%s - %s' % (self.sigla, self.descricao)
		


