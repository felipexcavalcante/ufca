# -*- coding: utf-8
from django.contrib import admin
from django.shortcuts import render_to_response

from django.utils.safestring import mark_safe
from django.contrib.contenttypes.models import ContentType
from django import template
from django.contrib.admin import widgets
from django.db import models as models_

from patrimonio import models


LIST_PER_PAGE = 15

class NewAdmin(admin.ModelAdmin):
	list_per_page = LIST_PER_PAGE
	#actions = None

def imprimir_etiqueta(modeladmin, request, queryset):
    pass
    
imprimir_etiqueta.short_description = u'Imprimir etiqueta'

def movimentar(modeladmin, request, queryset):
    pass
    
movimentar.short_description = u'Movimentar'


class MaterialAdmin(NewAdmin):
	list_display = ('numero', 'classificacao', 'incorporacao', )
	search_fields = ['incorporacao__processo', 'incorporacao__empenho', 'numero', 'classificacao__classificacao', 'incorporacao__documento', 'classificacao__descricao', 'observacao']
	list_filter = ('estado', )
	readonly_fields = ('numero','classificacao', 'incorporacao', 'valor')
	
	actions = [imprimir_etiqueta, movimentar]
	actions_selection_counter = True
	
	save_as = True

class ClassificacaoAdmin(NewAdmin):
	list_display = ('classificacao', 'descricao')
	search_fields = ['classificacao', 'descricao']


class ItemDeIncorporacaoAdmin(admin.TabularInline):
    model = models.ItemDeIncorporacao
    extra = 5
    
    
####################################
def gerar_materiais(modeladmin, request, queryset):
    
    for inc in queryset:
        inc.cadastrouositens = True
        inc.save()
        for item in inc.itemdeincorporacao_set.all():
            models.Material.adiciona(item)
    
gerar_materiais.short_description = u'Gerar Materiais'

class IncorporacaoAdmin(NewAdmin):
	list_display = ('cadastrouositens', 'tipo', 'empenho', 'processo', 'documento', 'total', 'datadoemp', 'datadainc')
	list_display_links = ('tipo', 'empenho')
	search_fields = ['empenho', 'processo', 'documento']
	date_hierarchy = 'datadainc'
	list_filter = ('cadastrouositens', 'tipo',)
	#list_editable = ('datadainc', 'processo')
	inlines = [ItemDeIncorporacaoAdmin]
	exclude = ('cadastrouositens',)
	readonly_fields = ('total',)


	actions = [gerar_materiais]
	actions_selection_counter = True
	

class SetorUFCAdmin(NewAdmin):
	list_display = ('sigla', 'descricao',)
	search_fields = ['descricao',]

'''

class ItemDeMovimentacaoInline(admin.TabularInline):
    model = models.ItemDeMovimentacao
    extra = 1

class MovimentacaoAdmin(NewAdmin):
	list_display = ('id', 'data', 'tipo', 'responsavel',)
	search_fields = ['responsavel',]
	inlines = [ItemDeMovimentacaoInline]

'''

admin.site.register(models.Classificacao, ClassificacaoAdmin)
admin.site.register(models.Incorporacao, IncorporacaoAdmin)
admin.site.register(models.Material, MaterialAdmin)
admin.site.register(models.SetorUFC, SetorUFCAdmin)
#admin.site.register(models.Movimentacao, MovimentacaoAdmin)

