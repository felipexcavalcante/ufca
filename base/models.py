# -*- coding: utf-8
from django.db import models
		
class Setor(models.Model):
	descricao = models.CharField(max_length=100, verbose_name='descrição')

	class Meta:
		verbose_name_plural = u'Setores'

	def __unicode__(self):
		return self.descricao
		
		

class Local(models.Model):
    USOS = (('E', u'Escritório'), ('A', u'Sala de Aula'), ('B', u'Banheiro'), ('C', u'Corredor'), ('O', u'Outros'), )
    identificacao = models.CharField(u'identificação', max_length=10)
    descricao = models.CharField(u'descrição', max_length=30)
    complemento = models.CharField(max_length=100, null=True, blank=True)
    area = models.DecimalField(u'área', max_digits=12, decimal_places=2, null=True, blank=True, )
    capacidade = models.IntegerField(null=True, blank=True, help_text=u'Quantidade de carteiras')
    uso = models.CharField(max_length=1, choices=USOS)

    class Meta:
        verbose_name_plural = u'Locais'

    def __unicode__(self):
        if self.complemento:
            return "%s - %s" % (self.descricao, self.complemento)
        else:
            return self.descricao 


class Pessoa(models.Model):
    CARGOS = (('S', 'Técnico Administrativo'), ('P', 'Professor'), ('T', 'Terceirizado'))

    nome = models.CharField(max_length=100)
    siape = models.CharField(max_length=9, null=True, blank=True)
    cpf = models.CharField(u'CPF', max_length=14, null=True, blank=True)
    cargo = models.CharField(choices=CARGOS, max_length=1)
    telefones = models.CharField(max_length=50, null=True, blank=True)
    email = models.CharField(u'e-mail', max_length=100, null=True, blank=True)

    class Meta:
        ordering = ['nome']

    def __unicode__(self):
        return self.nome


#==========================================================
# Fornecedor
#==========================================================
class Fornecedor(models.Model):
    nome = models.CharField(max_length=50, unique=True)
    razao_social = models.CharField(u'Razão Social', max_length=100, blank=True, null=True)
    cnpj = models.CharField(u'CNPJ', max_length=17, blank=True, null=True)
    observacao = models.TextField(u'Observação', null=True, blank=True)
    
    class Meta:
        verbose_name = 'fornecedor'
        verbose_name_plural = 'fornecedores'
        ordering = ['nome']
        
    def __unicode__(self):
        return self.nome



