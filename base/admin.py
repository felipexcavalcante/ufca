# -*- coding: utf-8
from django.contrib import admin
from django.shortcuts import render_to_response

from django.utils.safestring import mark_safe
from django.contrib.contenttypes.models import ContentType
from django import template
from django.contrib.admin import widgets

from base import models

LIST_PER_PAGE = 15

class NewAdmin(admin.ModelAdmin):
	list_per_page = LIST_PER_PAGE
	actions = None

class LocalAdmin(NewAdmin):
	list_display = ('descricao', 'complemento', 'uso', 'area', 'capacidade', )
	search_fields = ['descricao', 'complemento', 'uso']
	list_filter = ['uso']

class SetorAdmin(NewAdmin):
	list_display = ('descricao',)
	search_fields = ['descricao',]

class PessoaAdmin(NewAdmin):
	list_display = ('siape', 'nome', 'cargo', 'telefones', 'email')
	search_fields = ['siape', 'nome',]
	list_filter = ('cargo',)
	list_display_links = ('siape', 'nome')



admin.site.register(models.Setor, SetorAdmin)
admin.site.register(models.Local, LocalAdmin)
admin.site.register(models.Pessoa, PessoaAdmin)
admin.site.register(models.Fornecedor)

